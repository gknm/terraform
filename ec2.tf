provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "jenkins_instance" {
  instance_type = "t2.micro"
  vpc_security_group_ids = [ "sg-ddfc02f7" ]
  associate_public_ip_address = true
  tags = {
    Name = "jenkins-instance"
  }
  key_name = "personal"
  ami = "ami-04505e74c0741db8d"
  availability_zone = "us-east-1c"
  subnet_id = "subnet-d1a24ff0"
}
